require('./gulp/configs/files');

var gulp = require('gulp'),
  helpers = require('./gulp/helpers.js'),
  dbAction = (helpers.plugins.util.env.loadDb) ? ['extend-db:loadDb'] : [];

gulp.task("html", helpers.getTask({taskName: 'html'}));
gulp.task("scripts", helpers.getTask({taskName: 'scripts'}));
gulp.task("tests", helpers.getTask({taskName: 'tests'}));
gulp.task('sass', helpers.getTask({taskName: 'styles'}));
gulp.task("webpack-server", helpers.getTask({taskName: 'webpack-server'}));

gulp.task('extend-db:loadDb',['load-db'], helpers.getTask({taskName: 'extend-db'}));
gulp.task('load-db', helpers.getTask({taskName: 'load-db'}));
gulp.task('save-db', helpers.getTask({taskName: 'save-db'}));
gulp.task('start-mock-server',dbAction, helpers.getTask({taskName: 'mock-server'}));
