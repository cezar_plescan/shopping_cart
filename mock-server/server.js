var jsonServer = require('json-server'),
  files = require('../gulp/configs/files'),
  gutil = require('gulp-util'),
  server = jsonServer.create(),
  router;

// Set default middlewares (logger, static, cors and no-cache)
server.use(jsonServer.defaults());
router = jsonServer.router(global.files.database.mainFile);
server.use(router);

server.listen(4444)
console.log('Mock server is listening at port 4444');
