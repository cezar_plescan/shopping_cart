module.exports = function(gulp, plugins) {
  return function() {
    return gulp.src(global.files.mocks.all)
      .pipe(plugins.extend('clone.json', false))
      .pipe(gulp.dest(global.files.database.cloneDestFolder));
  }
}
