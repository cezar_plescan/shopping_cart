var path = require('path'),
  webpack = require('webpack');

module.exports = {
    context: global.files.projectRoot,
    entry: {
        app: [
            global.files.sourceFolderPath  + 'pages/' + global.files.js.main
        ],
        vendors: ['react', 'react-dom', 'history', 'react-router']
    },
    output: {
        path: files.buildFolderPath,
        publicPath: 'http://localhost:3000/',
        filename: global.files.js.bundle,
        chunkFilename: '[id].' + global.files.js.bundle
    },
    module: {
        preLoaders: [
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components|build|gulp)/,
            loader: 'eslint-loader'
        }
        ],
        loaders: [
        {
            test: /\.js?$/,
            exclude: /(node_modules|bower_components|build|gulp)/,
            loaders: ['react-hot', 'babel?presets[]=react,presets[]=es2015,presets[]=stage-0&plugins[]=transform-runtime&cacheDirectory']
        },
        {
            test: /\.scss$/,
            exclude: /(bower_components|build|gulp)/,
            loader: 'style!css!sass'
        },
        {
            test: /\.(svg|jpg|png)$/,
            loader: 'file-loader'
        },
        {
            test: /\.(svg|png|jpg)$/,
            loader: 'url?limit=25000'
        }
        ]
    },
    eslint: {
        configFile: global.files.projectRoot + 'gulp/configs/.eslintrc',
        emitError: true,
        failOnError: true
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js', Infinity),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            mangle: {
                dedupe: true,
                minimize: true,
                except: ['$', 'exports', 'require']
            }
        })
    ]
};
