var path = require('path'),
  projectRoot = path.join(__dirname, '/../../');

global.files = {
  projectRoot: projectRoot,
  sourceFolder: 'app',
  sourceFolderPath: projectRoot + 'app/',
  buildFolder: 'build',
  buildFolderPath: projectRoot + 'build/',
  serverScript: projectRoot + 'mock-server/server.js',
  database:{
    destFolder: projectRoot + 'build/database/',
    cloneDestFolder: projectRoot + 'build/database/clone/',
    backupsFolder: projectRoot + 'database-backups/',
    mainFile: projectRoot + 'build/database/db.json',
    cloneFile: projectRoot + 'build/database/clone/clone.json',
  },
  js: {
    all: projectRoot + 'app/**/js/*.js',
    main: 'main.js',
    mainPath: projectRoot + 'pages/main.js',
    bundle: 'main.bundle.js'
  },
  tests: {
    all: projectRoot + 'app/**/tests/*spec.js'
  },
  css: {
    all: 'app/**/*.scss',
    buildFile: 'app.css'
  },
  html: {
    all: 'app/**/*.html'
  },
  mocks: {
    all: projectRoot + 'app/**/mocks/*.json'
  }
};
