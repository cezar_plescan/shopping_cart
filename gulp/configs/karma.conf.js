var webpack = require('webpack'),
  path = require('path'),
  glob = require('glob');

var testsFile = path.join(global.files.projectRoot, 'app/common/tests/tests.webpack.js'),
  phantomJsShim = path.join(global.files.projectRoot, 'app/common/tests/phantomjs-shim.js'),
  node_modules = path.resolve(__dirname, '../../node_modules'),
  pathToReact = path.resolve(node_modules, 'react/dist/react.js'),
  pathToReactDom = path.resolve(node_modules, 'react-dom/dist/react-dom.js'),
  pathToReactRouter = path.resolve(node_modules, 'react-router/umd/ReactRouter.min.js'),
  pathToHistory = path.resolve(node_modules, 'history/umd/history.min.js');

var testsFolders = glob.sync(global.files.projectRoot + 'app/**/tests').map(function(path) {
  return path.replace(/\//g, '\\');
});

var karmaConfig = {
  browsers: [ 'PhantomJS' ], //run in PhantomJS
  singleRun: false, //just run once by default
  frameworks: [ 'jasmine' ], //use the jasmine test framework
  files: [
     phantomJsShim,
     testsFile
  ],
  preprocessors: {},
  coverageReporter: {
    dir : global.files.projectRoot + 'coverage',
    subdir: 'reports',
    reporters: [
      {type: 'lcov'},
      {type: 'cobertura', file: 'client-coverage.xml'}
    ]
  },
  plugins: [
    'karma-mocha-reporter',
    'karma-coverage',
    'karma-jasmine',
    'karma-phantomjs-launcher',
    'karma-chrome-launcher',
    'karma-webpack'
  ],
  reporters: ['mocha', 'coverage'], //report results in this format
  webpack: { //kind of a copy of your webpack config
    devtool: 'inline-source-map', //just do inline source maps instead of the default
    resolve: {
      alias: {
        'react': pathToReact,
        'react-dom': pathToReactDom,
        'react-router': pathToReactRouter,
        'history': pathToHistory
      }
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components|build|gulp)/,
          loaders: ['babel?cacheDirectory']
        }
      ],
      postLoaders:[
        {
          test: /\.js$/,
          exclude: [/node_modules/, /bower_components/, /build/, /gulp/].concat(testsFolders),
          loaders: ['isparta-instrumenter']
        }
      ],
      noParse: [pathToReact, pathToReactDom]
    }
  }
};

karmaConfig.preprocessors[testsFile] = ['webpack'];
karmaConfig.preprocessors[global.files.tests.all] = ['webpack', 'coverage'];

module.exports = function (config) {
  config.set(karmaConfig);
};
