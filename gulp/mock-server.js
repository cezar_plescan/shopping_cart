var fs = require('fs'),
  helpers = require('./helpers');

module.exports = function(gulp, plugins) {
  gulp.task('extend-mocks', helpers.getTask({taskName: 'extend-mocks'}));
  gulp.task('extend-db', helpers.getTask({taskName: 'extend-db'}));

  function startServer(){
    return plugins.nodemon({
      verbose: false,
      script: global.files.serverScript,
      ext:'json',
      watch: [global.files.mocks.all]
    }).on('restart',['extend-mocks','extend-db']).on('crash',['save-db']);
  }

  return function() {
    var dbfile = global.files.database.mainFile;

    if (!fs.existsSync(dbfile)) {
      gulp.task('extend-db:mocks',['extend-mocks'], helpers.getTask({taskName: 'extend-db'}));
      gulp.run('extend-db:mocks', function(){
        startServer();
      });
      return;
    }

    startServer();
  };
}
