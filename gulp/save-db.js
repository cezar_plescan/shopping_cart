module.exports = function(gulp, plugins) {
  return function() {
    var filename = plugins.util.env.dbName,
      now = new Date().getTime();

    filename = filename ?  [filename, '.json'].join('') : ["db-backup-", now, '.json'].join('');

    return gulp
      .src(global.files.database.mainFile)
      .pipe(plugins.extend(filename), false)
      .pipe(gulp.dest(global.files.database.backupsFolder));
  }
}
