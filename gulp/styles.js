module.exports = function(gulp, plugins) {
  return function () {
    var sourceMapConfig = {
      includeContent: false,
      sourceRoot: 'source'
    };

    return plugins.rubySass([global.files.css.all], { style: 'expanded' })
      .pipe(plugins.autoprefixer('last 2 version'))
      .pipe(!plugins.util.env.production ? plugins.sourcemaps.write() : plugins.util.noop())
      .pipe(!plugins.util.env.production ? plugins.sourcemaps.write('maps', sourceMapConfig) : plugins.util.noop())
      .pipe(gulp.dest(global.files.buildFolder));
  }
}