var Server = require('karma').Server,
  path = require('path');

module.exports = function(gulp, plugins) {
  return function(done) {
    var serverConfig = {
      configFile: __dirname + '/configs/karma.conf.js',
      singleRun: false
    },
    phantomJsShim = path.join(global.files.projectRoot, 'app/common/tests/phantomjs-shim.js');

    function endsWith(str, sufix) {
      return str.indexOf(sufix, this.length - sufix.length) !== -1;
    }

    function getTestFiles(testsToRun) {
      if(!testsToRun) {
        return global.files.tests.all;
      }

      if (endsWith(testsToRun, '.js')) {
        return testsToRun;
      }

      if (endsWith(testsToRun, 'tests')) {
        return testsToRun + '/*.js';
      }

      return testsToRun + '/**/tests/*.js';
    }

    if(plugins.util.env.tests) {
      var testFiles = getTestFiles(plugins.util.env.tests);
      serverConfig.files = [];
      serverConfig.files.push(phantomJsShim);
      serverConfig.files.push(path.join(global.files.sourceFolderPath, testFiles));
    }

    new Server(serverConfig, function(){done();}).start();
  };
};
