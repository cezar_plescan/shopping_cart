fs = require('fs');

module.exports = function(gulp, plugins) {
  return function() {
    var dbmock,
      filename = plugins.util.env.loadDb,
      backups;

    if (typeof filename !== 'string') {
      plugins.util.log('Please specify which file to load...');
      return;
    }
    filename = [filename, '.json'].join('');
    dbmock = global.files.database.backupsFolder + filename;

    if (!fs.existsSync(dbmock)) {
      plugins.util.log(filename + ' is not an existing database file...');
      return;
    }

    return gulp
      .src(dbmock)
      .pipe(plugins.extend('clone.json', false))
      .pipe(gulp.dest(global.files.database.cloneDestFolder))
  }
}
