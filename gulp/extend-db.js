var fs = require("fs");

module.exports = function(gulp, plugins) {
  return function() {
    var flags = plugins.util.env,
      db = global.files.database.mainFile,
      clone = global.files.database.cloneFile,
      deep = true,
      sources = [db, clone];

    if(!fs.existsSync(db) || flags.loadDb){
      sources.splice(0,1);
      deep = false;
    }

    return gulp.src(sources)
      .pipe(plugins.extend('db.json', deep))
      .pipe(gulp.dest(global.files.database.destFolder));

    // remove the reset flag so the next extend will be deep
    if(flags.loadDb){
      flags.loadDb = false;
    }
  }
}
